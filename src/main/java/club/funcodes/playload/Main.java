// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.playload;

import static org.refcodes.cli.CliSugar.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.refcodes.archetype.C2Helper;
import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ArrayOption;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.Literal;
import org.refcodes.data.Scheme;
import org.refcodes.exception.BugException;
import org.refcodes.exception.Trap;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.p2p.NoSuchDestinationException;
import org.refcodes.p2p.alt.serial.AcknowledgeMode;
import org.refcodes.p2p.alt.serial.SerialP2PMessage;
import org.refcodes.p2p.alt.serial.SerialP2PMessageConsumer;
import org.refcodes.p2p.alt.serial.SerialP2PTransmissionMetrics;
import org.refcodes.p2p.alt.serial.SerialPeer;
import org.refcodes.properties.Properties;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.rest.RestRequestConsumer;
import org.refcodes.rest.RestRequestEvent;
import org.refcodes.rest.RestResponse;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.rest.RestfulHttpServer;
import org.refcodes.runtime.Execution;
import org.refcodes.serial.alt.tty.Parity;
import org.refcodes.serial.alt.tty.StopBits;
import org.refcodes.serial.alt.tty.TtyPort;
import org.refcodes.serial.alt.tty.TtyPortHubSingleton;
import org.refcodes.serial.alt.tty.TtyPortMetrics;
import org.refcodes.serial.ext.handshake.HandshakeTransmissionMetrics;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.InternalServerErrorException;
import org.refcodes.web.MediaType;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.UrlBuilder;

/**
 * Tool for communication, P2P and encryption as well as steganography.
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int DEFAULT_DATA_BITS = 8;
	private static final int DEFAULT_BAUD_RATE = 9600;

	private static final String TITLE = "<PLAΨL0AD>";
	private static final String NAME = "playload";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "Peer-to-Peer (P2P) command line transport system for exchanging messages between participating parties (peers) over serial TTY (COM) ports.";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/teletype_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );

	private static final String MESSAGE_PROPERTY = "message";
	private static final String ATTACH_PROPERTY = "attach";
	private static final String FORWARD_PROPERTY = "forward";
	private static final String LOCATOR_PROPERTY = "locator";
	private static final String SOURCE_PROPERTY = "source";
	private static final String DESTINATION_PROPERTY = "destination";
	private static final String ACKNOWLEDGE_MODE_PROPERTY = "peers/acknowledge/mode";
	private static final String ACKNOWLEDGE_RETRY_NUMBER_PROPERTY = "peers/acknowledge/retryNumber";
	private static final String ACKNOWLEDGE_TIMEOUTIN_PROPERTY = "peers/acknowledge/timeoutInMs";
	private static final String REPLY_RETRY_NUMBER_PROPERTY = "peers/reply/retryNumber";
	private static final String REPLY_TIMEOUTIN_PROPERTY = "peers/reply/timeoutInMs";
	private static final String STOP_BITS_PROPERTY = "peers/ports/stopBits";
	private static final String PARITY_PROPERTY = "peers/ports/parity";
	private static final String DATA_BITS_PROPERTY = "peers/ports/dataBits";
	private static final String BAUD_PROPERTY = "peers/ports/baud";
	private static final String PORTS_PROPERTY = "peers/ports";
	private static final String PORTS_PATTERN_PROPERTY = "peers/ports/pattern";
	private static final String PORTS_COUNT_PROPERTY = "peers/ports/count";
	private static final String LIST_PROPERTY = "list";

	private static final int MAX_OPEN_FORWARD_DAEMON_RETRY_COUNT = 5;
	private static final String C2_FORWARD_ENDPOINT = "/forward";
	private static final String C2_PORT_PROPERTY = "port";
	private static final String DESTINATION_QUERY_FIELD = "destination";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static C2Helper _c2Helper = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// See "http://www.refcodes.org/refcodes/refcodes-cli" |-->
		final Flag theListPortsFlag = flag( 'l', "list", LIST_PROPERTY, "List all detected TTY (COM) serial ports." );
		final ArrayOption<String> theTtyPortsArg = asArray( stringOption( 'p', "port", PORTS_PROPERTY, "The COM (serial) port(s) to attach." ) );
		final IntOption theBaudArg = intOption( 'b', "baud", BAUD_PROPERTY, "The baud rate to use for the TTY (COM) serial port(s)." );
		final IntOption theDataBitsArg = intOption( "data-bits", DATA_BITS_PROPERTY, "The data bits to use for the TTY (COM) serial port(s) (usually a value of 7 or 8)." );
		final IntOption thePortsCountArg = intOption( "count", PORTS_COUNT_PROPERTY, "The number of ports to bind (if omitted, all according ports are bound)." );
		final EnumOption<AcknowledgeMode> theAckModeArg = enumOption( "ack", AcknowledgeMode.class, ACKNOWLEDGE_MODE_PROPERTY, "The acknowledge mode to use for transmissions: " + VerboseTextBuilder.asString( AcknowledgeMode.values() ) );
		final EnumOption<Parity> theParityArg = enumOption( "parity", Parity.class, PARITY_PROPERTY, "The parity to use for the TTY (COM) serial port(s): " + VerboseTextBuilder.asString( Parity.values() ) );
		final EnumOption<StopBits> theStopBitsArg = enumOption( "stop-bits", StopBits.class, STOP_BITS_PROPERTY, "The stop bits to use for the TTY (COM) serial port(s): " + VerboseTextBuilder.asString( StopBits.values() ) );
		final IntOption theLocatorArg = intOption( 'L', "locator", LOCATOR_PROPERTY, "The locator ID of the peer." );
		final IntOption theSrcArg = intOption( 's', "source", SOURCE_PROPERTY, "The locator ID of the source peer." );
		final IntOption theDestArg = intOption( 'd', "destination", DESTINATION_PROPERTY, "The locator ID of the destination peer." );
		final StringOption thePortsPatternArg = stringOption( "pattern", PORTS_PATTERN_PROPERTY, "The name pattern for the ports to bind (\"*\"=any characters, \"?\"=one character)." );
		final StringOption theMessageArg = stringOption( 'm', "message", MESSAGE_PROPERTY, "The message to be sent." );
		final ConfigOption theConfigArg = configOption();
		final Flag theForwardFlag = flag( 'f', "forward", FORWARD_PROPERTY, "Forward a request to the P2P network." );
		final Flag theAttachFlag = flag( 'a', "attach", ATTACH_PROPERTY, "Attach peer to P2P network." );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final Flag theQuietFlag = quietFlag();
		final Flag theDebugFlag = debugFlag();
		final Flag theForceFlag = forceFlag( false );
		final Flag theInitFlag = initFlag( false );
		final Flag theCleanFlag = cleanFlag( false );

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( theListPortsFlag, any ( theQuietFlag, theDebugFlag ) ),
			and( theAttachFlag, any ( theLocatorArg, theConfigArg, xor( theTtyPortsArg, and( thePortsPatternArg, any( thePortsCountArg ) ) ), theBaudArg, theDataBitsArg, theStopBitsArg, theParityArg, theAckModeArg, theQuietFlag, theDebugFlag, theForceFlag ) ),
			and( theForwardFlag, theSrcArg, theDestArg, theMessageArg, any (theQuietFlag, theDebugFlag, theForceFlag) ) ,
			and( theInitFlag, optional( theConfigArg, theQuietFlag ) ),
			and( theLocatorArg, theCleanFlag, any( theQuietFlag, theDebugFlag ) ),
			xor( theHelpFlag, and( theSysInfoFlag, any( theQuietFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "List all available TTY (COM) ports", theListPortsFlag ),
			example( "Attach peer to P2P network on given ports", theAttachFlag, theLocatorArg, theTtyPortsArg),
			example( "Attach peer to P2P network on specified ports", theAttachFlag, theLocatorArg, thePortsPatternArg),
			example( "Attach peer to P2P network using the given config", theAttachFlag, theConfigArg),
			example( "Forward data to P2P network", theSrcArg, theDestArg, theForwardFlag, theMessageArg),
			example( "Initialize default config file", theInitFlag ),
			example( "Initialize specific config file", theInitFlag, theConfigArg),
			example( "Clean the peer's lock file", theCleanFlag, theLocatorArg),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();
		final boolean isDebug = theArgsProperties.getBoolean( theDebugFlag );
		final boolean isForce = theArgsProperties.getBoolean( theForceFlag );
		final boolean isVerbose = theCliHelper.isVerbose();

		if ( !isVerbose ) {
			Execution.setJulLoggingStreams( System.out, System.err, Level.WARNING );
		}

		try {
			if ( theListPortsFlag.isEnabled() ) {
				listSerialPorts( isVerbose );
			}
			else if ( theAttachFlag.isEnabled() || theForwardFlag.isEnabled() || theCleanFlag.isEnabled() ) {
				final int theLocator = theArgsProperties.getInt( theForwardFlag.isEnabled() ? theSrcArg : theLocatorArg );
				_c2Helper = C2Helper.builder().withInstanceAlias( Integer.toString( theLocator ) ).withResourceClass( Main.class ).withLogger( LOGGER ).withVerbose( isVerbose ).withForce( isForce ).build();

				if ( theAttachFlag.isEnabled() ) {
					if ( isVerbose ) {
						LOGGER.printSeparator();
						LOGGER.info( "Attaching peer <" + theLocator + ">..." );
					}
					final int theBaudRate = theArgsProperties.getIntOr( theBaudArg, DEFAULT_BAUD_RATE );
					final int theDataBits = theArgsProperties.getIntOr( theDataBitsArg, DEFAULT_DATA_BITS );
					final Parity theParity = theArgsProperties.getEnumOr( theParityArg, Parity.NONE );
					final StopBits theStopBits = theArgsProperties.getEnumOr( theStopBitsArg, StopBits.AUTO );
					final AcknowledgeMode theAckMode = theArgsProperties.getEnumOr( theAckModeArg, AcknowledgeMode.ON );
					final int theAckRetryNumber = theArgsProperties.getIntOr( ACKNOWLEDGE_RETRY_NUMBER_PROPERTY, HandshakeTransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER );
					final long theAckTimeout = theArgsProperties.getLongOr( ACKNOWLEDGE_TIMEOUTIN_PROPERTY, HandshakeTransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS );
					final int theReplyRetryNumber = theArgsProperties.getIntOr( REPLY_RETRY_NUMBER_PROPERTY, HandshakeTransmissionMetrics.DEFAULT_REPLY_RETRY_NUMBER );
					final long theReplyTimeout = theArgsProperties.getLongOr( REPLY_TIMEOUTIN_PROPERTY, HandshakeTransmissionMetrics.DEFAULT_REPLY_TIMEOUT_IN_MS );
					final String[] thePortNames = theArgsProperties.getArrayOr( theTtyPortsArg, null );
					final String thePortsPattern = theArgsProperties.getString( thePortsPatternArg );
					final int thePortsCount = theArgsProperties.getIntOr( thePortsCountArg, -1 );
					if ( thePortsCount != -1 && thePortsCount <= 0 ) {
						throw new IllegalArgumentException( "The ports count <" + thePortsCount + "> property <" + PORTS_COUNT_PROPERTY + "> must be greater than <0>!" );
					}
					final TtyPortMetrics thePortMetrics = TtyPortMetrics.builder().withBaudRate( theBaudRate ).withDataBits( theDataBits ).withParity( theParity ).withStopBits( theStopBits ).build();
					final TtyPort[] theTtyPorts = thePortNames != null && thePortNames.length != 0 ? toTtyPorts( thePortNames, thePortMetrics, isVerbose ) : toTtyPorts( thePortsPattern, thePortsCount, thePortMetrics, isVerbose );
					final SerialP2PTransmissionMetrics theTransmissionMetrics = SerialP2PTransmissionMetrics.builder().withAcknowledgeMode( theAckMode ).withAcknowledgeRetryNumber( theAckRetryNumber ).withAcknowledgeTimeoutMillis( theAckTimeout ).withReplyRetryNumber( theReplyRetryNumber ).withReplyTimeoutMillis( theReplyTimeout ).build();

					if ( isVerbose ) {
						LOGGER.printSeparator();
						LOGGER.info( "The transmission acknowledge mode is set to <" + theTransmissionMetrics.getAcknowledgeMode() + ">..." );
						LOGGER.info( "Applying <" + theTransmissionMetrics.getReplyTimeoutMillis() + "> ms reply timeout each for a total of <" + theTransmissionMetrics.getReplyRetryNumber() + "> retries..." );
						if ( theTransmissionMetrics.getAcknowledgeMode() == AcknowledgeMode.ON ) {
							LOGGER.info( "Applying <" + theTransmissionMetrics.getAcknowledgeTimeoutMillis() + "> ms acknowledge timeout each for a total of <" + theTransmissionMetrics.getAcknowledgeRetryNumber() + "> retries..." );
						}
					}

					final SerialPeer thePeer = new SerialPeer( theLocator, new LoggingMessageConsumer( isVerbose, isDebug ), theTransmissionMetrics, theTtyPorts );

					final Properties theProperties = _c2Helper.readLockFile( isVerbose );
					if ( theProperties != null ) {
						final Integer thePort = theProperties.getInt( C2_PORT_PROPERTY );
						if ( thePort != null && !PortManagerSingleton.getInstance().isPortAvaialble( thePort ) ) {
							throw new IllegalStateException( "Cannot start daemon as the port <" + thePort + "> reserved by the lockfile is already in use!" );
						}
					}
					RestfulHttpServer theRestServer = null;
					int count = 1;
					int thePort;
					do {
						thePort = PortManagerSingleton.getInstance().bindAnyPort();
						try {
							theRestServer = new HttpRestServer().withOpen( thePort );
						}
						catch ( IOException e ) {
							if ( count > MAX_OPEN_FORWARD_DAEMON_RETRY_COUNT ) {
								throw e;
							}
						}
						count++;
					} while ( theRestServer == null );

					_c2Helper.writeLockFile( new PropertiesBuilderImpl().withPutInt( C2_PORT_PROPERTY, thePort ), isVerbose );
					theRestServer.onPost( toForwardHttpEndpoint(), new MessageRequestConsumer( thePeer, isVerbose, isDebug ) ).open();

				}
				else if ( theForwardFlag.isEnabled() ) {
					final int theDestination = theArgsProperties.getInt( theDestArg );
					final String theMessage = theArgsProperties.get( theMessageArg );
					forwardMessage( theLocator, theDestination, theMessage, isVerbose, isDebug );
				}
				else if ( theCleanFlag.isEnabled() ) {
					_c2Helper.deleteLockFile( isVerbose );
				}
				else {
					throw new BugException( "We should never end up here, please check your command line args!" );
				}
			}
			else {
				throw new BugException( "We should never end up here, please check your command line args!" );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected static TtyPort[] toTtyPorts( String[] aPortNames, TtyPortMetrics aPortMetrics, boolean isVerbose ) throws IOException {
		final TtyPort[] theTtyPorts = new TtyPort[aPortNames.length];

		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Initializing ports with <" + aPortMetrics.getBaudRate() + "> baud, <" + aPortMetrics.getDataBits() + "> data bits, <" + aPortMetrics.getStopBits() + "> stop bits and parity <" + aPortMetrics.getParity() + ">..." );
		}
		for ( int i = 0; i < theTtyPorts.length; i++ ) {
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "Binding port <" + aPortNames[i] + ">..." );
			}
			theTtyPorts[i] = ( TtyPortHubSingleton.getInstance().toPort( aPortNames[i] ).withOpen( aPortMetrics ) );
		}
		return theTtyPorts;
	}

	protected static TtyPort[] toTtyPorts( String aPortsPattern, int aPortsCount, TtyPortMetrics aPortMetrics, boolean isVerbose ) throws IOException {
		final List<TtyPort> theTtyPorts = new ArrayList<>();
		final String thePortsPattern = aPortsPattern != null ? aPortsPattern.replace( "*", ".*" ).replace( "?", "." ) : null;
		String ePortName;
		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Initializing ports with <" + aPortMetrics.getBaudRate() + "> baud, <" + aPortMetrics.getDataBits() + "> data bits, <" + aPortMetrics.getStopBits() + "> stop bits and parity <" + aPortMetrics.getParity() + ">..." );
		}
		for ( TtyPort ePort : TtyPortHubSingleton.getInstance().ports() ) {
			ePortName = ePort.getAlias();
			try {
				if ( isVerbose ) {
					LOGGER.printSeparator();
				}
				if ( thePortsPattern != null && thePortsPattern.length() != 0 ) {
					if ( ePortName.matches( thePortsPattern ) ) {
						theTtyPorts.add( ePort.withOpen( aPortMetrics ) );
					}
					if ( isVerbose ) {
						LOGGER.info( "Binding port <" + ePortName + ">..." );
					}
				}
				else {
					theTtyPorts.add( ePort.withOpen( aPortMetrics ) );
					if ( isVerbose ) {
						LOGGER.info( "Binding port <" + ePortName + ">..." );
					}
				}
				if ( aPortsCount != -1 && theTtyPorts.size() == aPortsCount ) {
					break;
				}
			}
			catch ( IOException e ) {
				if ( isVerbose ) {
					LOGGER.warn( "Skipping port <" + ePortName + "> (probably already bound) as of: " + Trap.asMessage( e ) );
				}
			}
		}

		if ( theTtyPorts.isEmpty() ) {
			final String thePortsPatternMessage = aPortsPattern != null && aPortsPattern.length() != 0 ? "ports pattern \"" + aPortsPattern + "\" property <" + PORTS_PATTERN_PROPERTY + "> " : "";
			final String thePortsCountMessage = aPortsCount != -1 ? "ports count <" + aPortsCount + "> property <" + PORTS_COUNT_PROPERTY + "> " : "";
			throw new IllegalArgumentException( "No ports found for the provided " + thePortsPatternMessage + ( thePortsPatternMessage != null && thePortsCountMessage != null ? "and " : "" ) + thePortsCountMessage + "arguments!" );
		}
		if ( aPortsCount != -1 && theTtyPorts.size() != aPortsCount ) {
			final String thePortsPatternMessage = aPortsPattern != null && aPortsPattern.length() != 0 ? "ports pattern \"" + aPortsPattern + "\" property <" + PORTS_PATTERN_PROPERTY + "> " : "";
			final String thePortsCountMessage = aPortsCount != -1 ? "ports count <" + aPortsCount + "> property <" + PORTS_COUNT_PROPERTY + "> " : "";
			throw new IllegalArgumentException( "Only found <" + theTtyPorts.size() + "> ports for the provided " + thePortsPatternMessage + ( thePortsPatternMessage != null && thePortsCountMessage != null ? "and " : "" ) + thePortsCountMessage + "arguments!" );
		}
		return theTtyPorts.toArray( new TtyPort[theTtyPorts.size()] );
	}

	private static void listSerialPorts( boolean isVerbose ) throws IOException {
		final TtyPort[] thePorts = TtyPortHubSingleton.getInstance().ports();
		if ( isVerbose ) {
			if ( thePorts.length != 0 ) {
				int theMaxWidth = 0;
				int eWidth;
				for ( TtyPort ePort : thePorts ) {
					eWidth = ePort.getAlias().length();
					if ( theMaxWidth < eWidth ) {
						theMaxWidth = eWidth;
					}
				}
				final HorizAlignTextBuilder theBuilder = new HorizAlignTextBuilder().withColumnWidth( theMaxWidth );
				for ( TtyPort ePort : thePorts ) {
					LOGGER.info( "[" + theBuilder.toString( ePort.getAlias() ) + "] " + ePort.getName() + ": \"" + ePort.getDescription() + "\" (" + ePort.getPortMetrics().getBaudRate() + " baud)" );
				}
			}
			else {
				LOGGER.info( "No serial (COM) ports found." );
			}
		}
		else {
			for ( TtyPort ePort : thePorts ) {
				System.out.println( ePort.getAlias() + "\t" + ePort.getPortMetrics().getBaudRate() + " baud\t" + ePort.getDescription() + "\t" + ePort.getName() );
			}
		}
	}

	protected static void forwardMessage( int aSource, int aDestination, String aMessage, boolean isVerbose, boolean isDebug ) throws IOException, ParseException, HttpStatusException {
		final Properties theLockProperties = _c2Helper.readLockFile( isVerbose );
		if ( theLockProperties != null ) {
			final int thePort = theLockProperties.getInt( C2_PORT_PROPERTY );
			if ( PortManagerSingleton.getInstance().isPortBound( thePort ) ) {
				if ( isVerbose ) {
					LOGGER.printSeparator();
					LOGGER.info( "Forwarding \"" + aMessage + "\" from peer <" + aSource + "> to peer <" + aDestination + ">..." );
				}
				final RestfulHttpClient theClient = new HttpRestClient().withBaseUrl( Scheme.HTTP, Literal.LOCALHOST.getValue(), thePort );
				final UrlBuilder theUrl = new UrlBuilder( toForwardHttpEndpoint() );
				theUrl.getQueryFields().put( DESTINATION_QUERY_FIELD, Integer.toString( aDestination ) );
				final RequestHeaderFields theHeadeFields = new RequestHeaderFields();
				theHeadeFields.putContentType( MediaType.TEXT_PLAIN );
				final RestResponse theResponse = theClient.doPost( theUrl, theHeadeFields, aMessage );
				if ( theResponse.getHttpStatusCode() == HttpStatusCode.NOT_FOUND ) {
					if ( isVerbose ) {
						LOGGER.warn( theResponse.getHttpBody() );
					}
					else {
						System.out.println( theResponse.getHttpBody() );
					}
				}
				else if ( theResponse.getHttpStatusCode().isErrorStatus() ) {
					throw theResponse.getHttpStatusCode().toHttpStatusException( "Failed to forward message \"" + aMessage + "\" from peer <" + aSource + "> to peer <" + aDestination + ">: " + theResponse.getHttpBody() );
				}
				else {
					if ( isVerbose ) {
						LOGGER.printSeparator();
						LOGGER.info( "Successfully dispatched \"" + aMessage + "\" from peer <" + aSource + "> to peer <" + aDestination + ">!" );
					}
					else {
						System.out.println( "Successfully dispatched \"" + aMessage + "\" from peer <" + aSource + "> to peer <" + aDestination + ">!" );
					}
				}
			}
		}
		else {
			throw new IllegalStateException( "The peer <" + aSource + "> seems not to be attached to the P2P network on this host!" );
		}
	}

	private static String toForwardHttpEndpoint() {
		return C2_FORWARD_ENDPOINT + _c2Helper.getInstanceAlias() != null ? ( "/" + _c2Helper.getInstanceAlias().toLowerCase() ) : "";
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static class MessageRequestConsumer implements RestRequestConsumer {

		private final SerialPeer _peer;
		private final boolean _isVerbose;
		private final boolean _isDebug;

		public MessageRequestConsumer( SerialPeer aPeer, boolean isVerbose, boolean isDebug ) {
			_peer = aPeer;
			_isVerbose = isVerbose;
			_isDebug = isDebug;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onRequest( RestRequestEvent aRequest, HttpServerResponse aResponse ) throws HttpStatusException {
			final String thePayload = aRequest.getHttpBody();
			final String theDestination = aRequest.getUrl().getQueryFields().getFirst( DESTINATION_QUERY_FIELD );
			if ( _isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "Dispatching \"" + thePayload + "\" from peer <" + _peer.getLocator() + "> to peer <" + theDestination + ">..." );
			}
			else {
				System.out.println( "Dispatching \"" + thePayload + "\" from peer <" + _peer.getLocator() + "> to peer <" + theDestination + ">..." );
			}
			try {
				_peer.sendMessage( Integer.valueOf( theDestination ), thePayload );
			}
			catch ( NoSuchDestinationException e ) {
				final String theMessage = "No such destination <" + theDestination + ">: Unable to dispatch \"" + thePayload + "\" from peer <" + _peer.getLocator() + "> to peer <" + theDestination + ">: " + e.toMessage();
				final String theWarning = Trap.asMessage( theMessage, e );
				if ( _isVerbose ) {
					if ( _isDebug ) {
						LOGGER.warn( theWarning, e );
					}
					else {
						LOGGER.warn( theWarning );
					}
				}
				else {
					System.out.println( theWarning );
					if ( _isDebug ) {
						e.printStackTrace();
					}
				}
				aResponse.getHeaderFields().putContentType( MediaType.TEXT_PLAIN );
				aResponse.setHttpStatusCode( HttpStatusCode.NOT_FOUND );
				aResponse.setResponse( theMessage );
			}
			catch ( NumberFormatException | IOException e ) {
				final String theMessage = "Unable to dispatch \"" + thePayload + "\" from peer <" + _peer.getLocator() + "> to peer <" + theDestination + ">!";
				final String theWarning = Trap.asMessage( theMessage, e );
				if ( _isVerbose ) {
					if ( _isDebug ) {
						LOGGER.warn( theWarning, e );
					}
					else {
						LOGGER.warn( theWarning );
					}
				}
				else {
					System.out.println( theWarning );
					if ( _isDebug ) {
						e.printStackTrace();
					}
				}
				throw new InternalServerErrorException( theMessage, e );
			}
		}
	}

	private static class LoggingMessageConsumer implements SerialP2PMessageConsumer {

		private final boolean _isVerbose;
		private final boolean _isDebug;

		public LoggingMessageConsumer( boolean isVerbose, boolean isDebug ) {
			_isVerbose = isVerbose;
			_isDebug = isDebug;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onP2PMessage( SerialP2PMessage aMessage, SerialPeer aPeer ) {
			try {
				final String theMessage = "Peer <" + aPeer.getLocator() + "> received \"" + aMessage.getPayload( String.class ) + "\" from peer <" + aMessage.getTail().getSource() + "> to peer <" + aMessage.getHeader().getDestination() + "> via " + VerboseTextBuilder.asString( aMessage.getTail().getHops() );
				if ( _isVerbose ) {
					LOGGER.printSeparator();
					LOGGER.info( theMessage );

				}
				else {
					System.out.println( theMessage );
				}
			}
			catch ( UnmarshalException e ) {
				final String theMessage = "Peer <" + aPeer.getLocator() + "> cannot unmarshal dispatch from peer <" + aMessage.getTail().getSource() + "> to peer <" + aMessage.getHeader().getDestination() + "> via " + VerboseTextBuilder.asString( aMessage.getTail().getHops() ) + ": " + e.getMessage();
				final String theWarning = Trap.asMessage( theMessage, e );
				if ( _isVerbose ) {
					if ( _isDebug ) {
						LOGGER.warn( theWarning, e );
					}
					else {
						LOGGER.warn( theWarning );
					}
				}
				else {
					System.out.println( theWarning );
					if ( _isDebug ) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
