module club.funcodes.playload {
	requires org.refcodes.archetype;
	requires org.refcodes.cli;
	requires org.refcodes.data;
	requires org.refcodes.exception;
	requires org.refcodes.logger;
	requires org.refcodes.runtime;
	requires org.refcodes.properties.ext.application;
	requires org.refcodes.web;
	requires org.refcodes.rest;
	requires org.refcodes.serial.alt.tty;
	requires org.refcodes.p2p.alt.serial;
}
